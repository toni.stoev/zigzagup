# ЗигЗаг наГоре . ZigZag Upwards

[ЗигЗаг наГоре](http://toni.cloudns.club/zigzagup/bg/) Редувано от-ляво-надясно и от-дясно-наляво писане и четене на текст от долу нагоре, известно още като [бустрофедон](https://bg.wikipedia.org/wiki/Бустрофедон).

[ZigZag Upwards](http://toni.cloudns.club/zigzagup/) Alternating left-to-right and right-to-left writing and reading of text from the bottom up, also known as [boustrophedon](https://en.wikipedia.org/wiki/Boustrophedon).
